# Code Test
Here are 4 tasks for your code test. Each task folder has their own readme for further information.

It's expected that you produce “production ready” code, this means that the code is not only accomplishing the task, 
but it’s resilient, performing and maintainable by anybody in the team.

Details are important, and you should treat your submission as if it were a pull request ready to go live and serve 
millions of users (read: automated tests, edge cases, error handling, good separation of concerns, good runtime and 
space complexities, documentation).

What will be evaluated of your submission:
* completeness
* quality
* correctness

## Constraints
* Create a `zip` file of your work
* Write your code either in `Go` or in `Python`