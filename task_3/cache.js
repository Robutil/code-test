var redisService = require('lib/redisService');

const cache;

function init(opts) {

  cache = redisService.init({
    port: opts.port,
    host: opts.host
  });

}

function get(key) {
  return cache.get(key);
}

function set(key, value, ttl) {
  return cache.set(key, value, ttl || opts.stdTTL);
}

function del(key) {
  return cache.del(key);
}

function flush() {
  return cache.flush();
}

function getCache(opts) {
  return {
    get: get,
    set: set,
    del: del,
    flush: flush
  };
}

module.exports = {
  init: init,
  getCache: getCache
};